var Hapi   = require('hapi');
var server = new Hapi.Server();
server.connection({ 
    host: 'localhost',
    port: 3000
});

server.register([{
    register: require('inert'),
    options: {}
}]);

var io = require('socket.io')(server.listener);

server.route([{
    method: 'GET',
    path  : '/',
    handler: function(request, reply) {
        reply.file('./index.html');
    }
}]);

io.on('connection', function(socket){
    socket.on('chat message', function(msg){
        console.log(msg);
        io.emit('chat message', msg);
    });
});

server.start(function(err) {
    if(!err) {
        console.log('Server is running at : ' + server.info.uri);
    } else {
        console.log(err);
    }
});